<?php

namespace App\Services\FormCodeGenerator;

use Nette,
	Nette\Object;


/**
 * FormCodeGenerator\ColumnEntity
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class ColumnEntity extends Nette\Object
{
	/** @var string */
	private $name;

	/** @var string */
	private $comment;

	/** @var string */
	private $inputType;

	/** @var bool */
	private $isNull;

	/** @var bool */
	private $isAutoIncrement;


	public function getName()
	{
		return $this->name;
	}


	public function getComment()
	{
		return $this->comment;
	}


	public function getInputType()
	{
		if ($this->isForeign() && $this->inputType == 'number') {
			return 'select';
		}
		if ($this->isPath() && $this->inputType == 'text') {
			return 'upload';
		}
		if ($this->isEmail() && $this->inputType == 'text') {
			return 'email';
		}

		return $this->inputType;
	}


	public function getNetteInputType()
	{
		$netteInputTypes = [
			'text', 'password', 'upload', 'multiUpload', 'hidden', 
			'checkbox', 'radioList', 'checkboxList', 'select', 'multiSelect', 
			'submit', 'button', 'image', 'textArea', 
		];

		if (in_array($this->getInputType(), $netteInputTypes)) {
			return $this->getInputType();
		}

		return 'text';
	}


	public function isNull()
	{
		return $this->isNull;
	}


	public function isAutoIncrement()
	{
		return $this->isAutoIncrement;
	}


	public function isForeign()
	{
		return (strrpos($this->name, '_id') === (strlen($this->name) - 3));  // column ends with "_id"
	}


	public function isPath()
	{
		return (strrpos($this->name, '_path') === (strlen($this->name) - 5));  // column ends with "_path"
	}


	public function isEmail()
	{
		return (strrpos($this->name, 'mail') === (strlen($this->name) - 4));  // column ends with "mail"
	}


	/********************* Setters *********************/


	public function setName($name)
	{
		$this->name = $name;
	}


	public function setComment($comment)
	{
		$this->comment = $comment;
	}


	public function setInputType($inputType)
	{
		$this->inputType = $inputType;
	}


	public function setIsNull($isNull)
	{
		$this->isNull = $isNull;
	}


	public function setIsAutoIncrement($isAutoIncrement)
	{
		$this->isAutoIncrement = $isAutoIncrement;
	}

}
