<?php

namespace App\Services\FormCodeGenerator;

use Nette,
	Nette\Object;


/**
 * FormCodeGenerator\Factory
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Factory extends Nette\Object
{
	/** @var Nette\DI\Container */
	protected $context;


	public function __construct(Nette\DI\Container $context)
	{
		$this->context = $context;
	}


	public function create()
	{
		return new Generator($this->context->parameters['tempDir'] . '/cache/latte');
	}

}
