<?php

namespace App\Services\FormCodeGenerator;

use Latte;
use Nette,
	Nette\Object,
	Nette\Utils\ArrayHash;


/**
 * FormCodeGenerator\Generator
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Generator extends Nette\Object
{
	/** @var string */
	protected $tableRegex = '/^[^:]+: (?P<name>[^\s]+)$/';

	/** @var string */
	protected $columnRegex = '/^(?P<name>[a-z0-9_]+)\t(?P<type>(([a-z]+\([^\)]+\))|(float)|(double)|(date)|(text))[^\t]*)(\t(?P<comment>.*))?$/i';

	/** @var array */
	protected $inputTypeTable = array(
		// db type => form input type
		'int' => 'number',
		'tinyint' => 'number',
		'float' => 'number',
		'double' => 'number',
		'decimal' => 'number',
		'date' => 'date',
		'datetime' => 'datetime',
		'varchar' => 'text',
		'text' => 'textArea',
		'enum' => 'select',
		'set' => 'select',
	);

	/** @var string */
	private $tempDir;


	public function __construct($tempDir)
	{
		$this->tempDir = $tempDir;
	}


	public function generate($input)
	{
		// 1) Prepare
		$input = ltrim($input);

		// 2) Parse
		$table = new TableEntity;
		$lines = explode("\n", $input);
		foreach ($lines as $i => $line) {
			// Table name?
			if ($i == 0 && preg_match($this->tableRegex, $line, $matches)) {
				$table->setName($matches['name']);
				continue;
			}

			// Column line?
			if (preg_match($this->columnRegex, $line, $matches)) {
				$column = new ColumnEntity;
				$column->setName($matches['name']);

				if (isset($matches['comment'])) {
					$column->setComment($matches['comment']);
				}

				$type = $this->parseColumnType($matches['type']);
				$column->setInputType(@$this->inputTypeTable[$type->dataType]?: 'text');
				$column->setIsNull($type->isNull);
				$column->setIsAutoIncrement($type->isAutoIncrement);

				$table->addColumn($column);
			}
		}

		// 3) Render
		$template = $this->createTemplate();
		$code = $template->renderToString(
			__DIR__ . '/template.latte', 
			array('table' => $table)
		);

		return rtrim($code);
	}


	protected function parseColumnType($typeString)
	{
		$result = new ArrayHash;

		// Data type
		if ($this->pregMatchCut('/^([a-z]+)\([^\)]+\) ?/i', $typeString, $matches)) {
			$result['dataType'] = $matches[1];
			// TODO enum values
		} elseif ($this->pregMatchCut('/^([a-z]+) ?/i', $typeString, $matches)) {
			$result['dataType'] = $matches[1];
		}

		// Unsigned (don't need that)
		$this->pregMatchCut('/unsigned ?/i', $typeString);

		// Zerofill (don't need that)
		$this->pregMatchCut('/zerofill ?/i', $typeString);

		// Is NULL ?
		$result['isNull'] = (bool) $this->pregMatchCut('/NULL ?/i', $typeString);

		// Is Auto Increment ?
		$result['isAutoIncrement'] = (bool) $this->pregMatchCut('/Auto Increment ?/i', $typeString);

		// Default value (don't need that)
		$this->pregMatchCut('/\[.*\] ?/i', $typeString);

		return $result;
	}


	/**
	 * This method behaves as preg_match function, 
	 * but found string is deleted from source. 
	 * 
	 * Found text is returned instead of 0/1 as original function. 
	 */
	protected function pregMatchCut($pattern, &$subject, &$matches = array())
	{
		if (preg_match($pattern, $subject, $matches)) {
			$subject = preg_replace($pattern, '', $subject);
			return $matches[0];
		}

		return 0;
	}


	protected function createTemplate()
	{
		$latte = new Latte\Engine;
		$latte->setTempDirectory($this->tempDir);

		$latte->addFilter('pascalCase', array('\Shake\Utils\Strings', 'toPascalCase'));
		$latte->addFilter('camelCase', array('\Shake\Utils\Strings', 'toCamelCase'));
		$latte->addFilter('underscoreCase', array('\Shake\Utils\Strings', 'toUnderscoreCase'));
		$latte->addFilter('singular', array('\Shake\Utils\Strings', 'singular'));
		$latte->addFilter('plural', array('\Shake\Utils\Strings', 'plural'));

		return $latte;
	}

}
