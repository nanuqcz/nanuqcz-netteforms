<?php

namespace App\Services\FormCodeGenerator;

use Nette,
	Nette\Object;


/**
 * FormCodeGenerator\TableEntity
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class TableEntity extends Nette\Object
{
	/** @var string */
	private $name = NULL;
	
	/** @var ColumnEntity[] */
	private $columns = array();


	public function setName($name)
	{
		$this->name = $name;
	}


	public function getName()
	{
		return $this->name? $this->name: '▓▓▓';
	}


	public function addColumn($column)
	{
		$this->columns[$column->name] = $column;
	}


	public function getColumns()
	{
		return $this->columns;
	}

}
