<?php

namespace App\Presenters;

use Nette,
	Nette\Application\UI\Form;
use App\Model,
	App\Services\FormCodeGenerator;


/**
 * Homepage Presenter
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class HomepagePresenter extends BasePresenter
{
	/** @inject @var FormCodeGenerator\Factory */
	public $formCodeGeneratorFactory;


	public function actionDefault()
	{
		$this->template->generatedCode = '';
	}


	protected function createComponentGeneratorForm()
	{
		$form = new Form;
		$form->addTextarea('adminer', 'Table structure from Adminer')
			->addRule(Form::FILLED, '`%label` should not be empty.');
		$form->addSubmit('process', 'Generate code');

		$form->onSuccess[] = array($this, 'processGenerateForm');
		return $form;
	}


	public function processGenerateForm(Form $form)
	{
		$generator = $this->formCodeGeneratorFactory->create();

		$this->template->generatedCode = $generator->generate($form->values->adminer);
	}

}
